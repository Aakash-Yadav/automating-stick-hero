from numpy import array
from PIL import Image
from os import system
from time import sleep
from pynput.mouse import Button, Controller

MOUSE = Controller()

class Automating(object):

    __slots__ = ("image_path",'image_to_np_array','red','out','new_array','new')

    def __init__(self,path) -> None:
        self.image_path = path;
        self.image_to_np_array = array(Image.open(self.image_path));

    def __get_the_dummy_image(self,v:int=0):
        self.new =  self.image_to_np_array[445:452,425:940];
        if(v):
            Image.fromarray(self.new).save('/media/akash/1C05-3B10/STICK_MAN/Images/gfg_dummy_pic.png')
            return self.new
        else:
            return self.new

    def solving_fuction_map_yellow(self,show_dummy:int=0):
        self.out = [];
        self.red = [];
        Dummy_array = self.__get_the_dummy_image(v=show_dummy)

        for arrays in range(len(Dummy_array)):
            black = 0;
            self.new_array = [list(x[:3]) for x in Dummy_array[arrays]]
            for index , i in enumerate(self.new_array):
                r,b,g = [int(x) for x in i];
                if(r==255 and b==0 and g==0):
                    self.red.append(index);
                elif ((r==240 and b==155 and g==25) or
                      (r==198 and b==201 and g==66) or
                      (r==55 and b==54 and g==56)   or
                      (r==108 and b==229 and g==248)and black>=18):
                    self.out.append(index);
                elif(r+g+b==0):
                    black = black+1;

        k =((list(dict.fromkeys(self.red))))
        end = (k[(len(k)//2)+1])
        start = ([x for x in self.out if x<200][-1]-5.5);
        return int((end-start)*2.1)/1000;

    @classmethod
    def play_game(cls,nth:int):
        sleep(2);
        print("Bot On")
        for i in range(nth):
            sleep(2.4);
            system("gnome-screenshot -f /media/akash/1C05-3B10/STICK_MAN/Images/app.png");
            class_value = cls("/media/akash/1C05-3B10/STICK_MAN/Images/app.png");
            key_press = (class_value.solving_fuction_map_yellow());
            print("distance = %s pixel"%(key_press));
            key_press = float(key_press*.98);
            MOUSE.press(Button.right);
            sleep(float(key_press));
            MOUSE.release(Button.right);
        return 1;
