
## Automating Online Games with Python: Stick Hero
![alt text](https://xp.io/storage/2u6NtyEt.png)
Stretch the stick in order to reach and walk on the platforms. Watch out! If the stick is not long enough, you will fall down!

[![Watch the video](Demo/run.mp4)